CC=gcc
DIR = ./includes/
CFLAGS= -I $(DIR) -c -lm

distancia: main.o point.o
	$(CC) -o distancia main.o point.o -I $(DIR)  -lm

main.o: main.c $(DIR)/point.h
	$(CC) $(CFLAGS) main.c

point.o: point.c $(DIR)/point.h
	$(CC) $(CFLAGS) point.c

clean:
	rm *o distancia
