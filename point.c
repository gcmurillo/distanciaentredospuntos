#include <point.h>
#include <math.h>

float calcularDistancia(Point p1, Point p2){

	return sqrt(pow((p1.x - p2.x), 2) + pow((p1.y - p2.y), 2) + pow((p1.z - p2.z), 2)); 


}

Point punto_medio(Point a,Point b){
 
	Point nuevo;
	nuevo.x = (a.x + b.x)/2;
	nuevo.y = (a.y + b.y)/2;
	nuevo.z = (a.z + b.z)/2;
	return nuevo;
} 
